package com.zuitt.wdc044.models;

import javax.persistence.*;

//mark this Java Object as a representation of a database table via @Entity annotation
@Entity
@Table(name = "posts")
public class Post {
    //indicate that this property represents the primary key;
    @Id
    //value for this property will be auto-incremented
    @GeneratedValue
    private Long id;

    // the class properties that represent table columns in a relational database and is annotated by Column
    @Column
    private String title;

    @Column
    private String content;

    // s04
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;
    //end of s04

    //CONSTRUCTOR
    public Post(){}

    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }

    public Post(String title, String content, User user){
        this.title = title;
        this.content = content;
        this.user = user;
    }

    // GETTERS AND SETTER
    public String getTitle(){
        return this.title;
    }

    public String getContent(){
        return this.content;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setContent(String content){
        this.content = content;
    }

    // s04
    public User getUser(){
        return this.user;
    }

    public void setUser(User user){
        this.user = user;
    }
    //end of s04
}
