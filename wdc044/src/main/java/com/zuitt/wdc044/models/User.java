package com.zuitt.wdc044.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    // s04 discussion
    @OneToMany(mappedBy = "user")
    @JsonIgnore
    public Set<Post> posts;
    // end of s04 discussion

    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    // GETTERS AND SETTER
    public String getUsername(){
        return this.username;
    }

    public String getPassword(){
        return this.password;
    }

    //post
    public Set<Post> getPosts(){
        return this.posts;
    }

    // end of post

    public void setUsername(String username){
        this.username = username;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public Long getId(){
        return id;
    }
}


/*
{
        "username": "json",
        "password": "pass"
        }*/
