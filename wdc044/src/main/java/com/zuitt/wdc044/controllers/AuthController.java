package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.JwtRequest;
import com.zuitt.wdc044.models.JwtResponse;
import com.zuitt.wdc044.services.JwtUserDetailsService;

// Insert code share
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
// end of codeshare

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

//@RestController - to tell the application that this class is a REST API Controller
@RestController
// this serves as the "cors" in the JavaScript
//@CrossOrigin - it disables CORS.
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtToken jwtToken;

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;

    // nagseset/nagdedefine ng route
    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    // "?" is not a good practice btw
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

        // load the username with the jwtUserDetails to add the username in the spring security scheme
        // Then the userDetails objects to create the token
        final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        // Generation of token
        final String token = jwtToken.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));

    }

    public void authenticate(String username, String password) throws Exception {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e){
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e){
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
